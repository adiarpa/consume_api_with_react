import React, { useEffect } from "react";

const RequestApiPokemon = () => {
    const [pokemon, setPokemon] = React.useState([]);
    const [pagination, setPagination] = React.useState(0);
    const [rowsPage, setRowsPage] = React.useState(10);
  
    const bringPokemon = async () => {
      try {
        const res = await fetch(`https://pokeapi.co/api/v2/pokemon/?limit=${rowsPage}&offset=${pagination*rowsPage}`);
  
        const data = await res.json();
        const pokemons = await Promise.all(
          data.results.map(async (e) => {
            const res = await fetch(e.url);
            return await res.json();
          })
        );
        setPokemon([...pokemons]);
      } catch (error) {
        console.log(error);      }
    };
  
    const back = () => {
      if (pagination > 0) {
        setPagination((pagination) => {
          return pagination - 1;
        });
      }
    };

    const next = () => {
      setPagination((pagination) => {
        return pagination + 1;
      });
    };
  
    useEffect(() => {
      bringPokemon();
    }, [pagination]);
  
    return (
      <div>
        <div class="container-i">
              <h1>PETICION AL API DE POKEMON</h1>
              <button onClick={bringPokemon}> Traer Pokemones</button>
              <button onClick={back}>Atrás</button>
              <button onClick={next}>Siguiente</button>
        </div>
  
        {pokemon.map((item) => {
          return (
            <div className="content" key={item.id}>
              <h4> [{item.id}] - NOMBRE: {item.name}, PESO: {item.weight} hg, ALTURA: {item.height} dm </h4>
              <img src={item.sprites["front_shiny"]} alt={item.name} />     
            </div>
          );
        })}

        <div class="container-ii">
              <button onClick={back}>Atrás</button>
              <button onClick={next}>Siguiente</button>
        </div>
        <br/>

        <footer>
          <h4>AUTOR: Ing. Adiel Arguelles Padilla</h4>
       </footer>

      </div>
    );
  };

export default RequestApiPokemon